clear all; close all; clc;

cmap=colormap(jet);

% Set file path
cd ../data/PEG10_DX10/6_5_PEG10_DX10/
% Read the relative solvation free energy
mu=load('mu.csv');
% Multiply the nanotube length (l)
lmu = mu*0.2;            % CNT length, l = 0.2 um

file=dir('*.txt');
N=numel(file);
for i=1:N
    s=strrep(file(i).name,'.txt','');
    S{i}=s;
end

cd ../../../scripts/

% Create empty variables for A, Y, and CPVP.
% Here, A is the matrix described in equation S5 in Supporting Information * lmu.
% Y is ln K, and CPVP is PVP concentration.
A=[]; Y=[]; CPVP = [];

for i = 1:N
    % Load the partition coefficients. The 1st column is ln(PVP
    % concentration) and 2nd column is ln(K).
    lnK = load([file(i).folder,'/', file(i).name]);
    l_data(i,1) = length(lnK); % length of each dataset
    temp_mat = zeros(l_data(i,1), N-1);
    if i ~= 1
        temp_mat(:,i-1) = ones(l_data(i,1), 1);
    end
    % Build matrix A
    A = [A ; ones(l_data(i,1),1)*lmu(i) exp(lnK(:,1))*lmu(i) temp_mat];
    % PVP concentration
    CPVP = [CPVP; exp(lnK(:,1))];
    % Y is lnK array
    Y=[Y ; lnK(:,2)];
end
% Calculate the beta column in eq S5 in Supporting Information.
b=A\Y;
beta=[0 ; b(3:length(b))];


%% Plot the master curve alpha using estimated beta
alpha = [];
figure(1)
for i = 1:N
    % Load the partition coefficients. The 1st column is ln(PVP
    % concentration) and 2nd column is ln(K).
    lnK = load([file(i).folder,'/', file(i).name]);
    % Calculated alpha based on the estimated beta
    alpha0 = -(lnK(:,2)/lmu(i)-beta(i)/lmu(i));
    
    % plot the original curve
    subplot(1,2,1)
    plot(lnK(:,1),lnK(:,2),'-o','DisplayName',S{i},'Color',cmap(floor(i*64/(N)),:));
    xlabel('lnPVP');
    ylabel('lnK');
    hold on
    
    % plot the master curve (delta alpha)
    subplot(1,2,2)
    plot(exp(lnK(:,1)),-alpha0,'-o','DisplayName',S{i},'Color',cmap(floor(i*64/(N)),:));
    xlabel('lnPVP');
    ylabel('-\Delta\alpha');
    hold on
    
    temp_beta = beta(i)*ones(length(alpha0),1); % to save the beta with alpha
    alpha=[alpha ; exp(lnK(:,1)) alpha0 temp_beta];
end

hold off


%% error calculation

So = (Y-A*b)'*(Y-A*b);
n = length(Y);
m = length(b);
B = ((n-m)/So)*A'*A;
Binv = inv(B);

z = 1.645; % 1.96 for 95%, 1.645 for 90% confidence interval
for i = 3:length(B)
    err(i-2,1) = z*sqrt(abs(Binv(i,i)));
end

So_each = zeros(length(l_data),1);

for i = 2:length(l_data)
    A_each = A(1+sum(l_data(1:i-1)):sum(l_data(1:i)),1:2);
    A_each = [A_each ones(l_data(i),1)];
    Y_each = Y(1+sum(l_data(1:i-1)):sum(l_data(1:i)));
    c_each = [b(1:2); b(i+1)];
    So_each(i,1) = (Y_each - A_each*c_each)'*(Y_each - A_each*c_each);
    B_each = ((l_data(i)-3)/So_each(i))*A_each'*A_each;
    det_B(i,1) = det(B_each);
    B_each_inv = inv(B_each);
    err2(i,1) = z*sqrt(abs(B_each_inv(3,3)));
end
