function F = combined(init1,xdata)
par=load('parameters_DNA_coated.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remember to change the values of beginx and endx in combined.m such that
% beginp = beginx and endp = endx
beginp=1;
endp=4;

% This is the list of default coefficients to be used for chiralities not
% between beginp and endp
a=[0.05 0 0 0 0 0 0 0 0 0 0 0 0.74]; 
n=length(xdata);
fit(1:n)=0;
for i=1:n
    for ipar = 1:length(par(:,1))
        
        %If the chirality is within beginp and endp
        if ipar>=beginp && ipar<=endp
            %For left part of peak
            if xdata(i)<par(ipar,1)
%               fit(i) = fit(i)+init1(ipar,1)*par(ipar,2)*1./(1+((xdata(i)-par(ipar,1))/par(ipar,6)).^2).^par(ipar,8);
                %Here bl is free variable
                fit(i) = fit(i)+init1(ipar,1)*par(ipar,2)*1./(1+((xdata(i)-init1(ipar,4))/init1(ipar,2)).^2).^par(ipar,8); 
            
            %For right part of peak    
            else
%               fit(i) = fit(i)+par(ipar,10)*init1(ipar,1)+init1(ipar,1)*par(ipar,3)*1./(1+((xdata(i)-par(ipar,1))/par(ipar,7)).^2).^par(ipar,9);
                %Here br is free variable
                fit(i) = fit(i)+par(ipar,10)*init1(ipar,1)+init1(ipar,1)*par(ipar,3)*1./(1+((xdata(i)-init1(ipar,4))/init1(ipar,3)).^2).^par(ipar,9);
            end
            
        %If the chirality is not between beginp and endp, use the default coefficients defined in matrix 'a'.    
        else
            %For left part of peak
            if xdata(i)<par(ipar,1)
                fit(i) = fit(i)+a(ipar)*par(ipar,2)*1./(1+((xdata(i)-par(ipar,1))/par(ipar,6)).^2).^par(ipar,8);
            %For right part of peak    
            else
                fit(i) = fit(i)+par(ipar,10)*a(ipar)+a(ipar)*par(ipar,3)*1./(1+((xdata(i)-par(ipar,1))/par(ipar,7)).^2).^par(ipar,9);
            end
        end
    end
end
fit(1:n)=fit(1:n)+init1(length(par(:,1))+1);
% fit(1:n)=fit(1:n)+a(length(par)+1);
F=fit;