function F = one_at_a_time(params,xdata,j)
par=load('parameters_DNA_coated.txt');
n=length(xdata);
fit(1:n)=0;
for i=1:n
    ipar=j;
    if xdata(i)<par(ipar,1)
        fit(i) = fit(i)+params(ipar)*par(ipar,2)*1./(1+((xdata(i)-par(ipar,1))/par(ipar,6)).^2).^par(ipar,8);
    else
        fit(i) = fit(i)+par(ipar,10)*params(ipar)+params(ipar)*par(ipar,3)*1./(1+((xdata(i)-par(ipar,1))/par(ipar,7)).^2).^par(ipar,9);
    end
end
fit(1:n)=fit(1:n)+params(length(par(:,1))+1);
% fit(1:n)=fit(1:n)+a(length(par)+1);
F=fit;
