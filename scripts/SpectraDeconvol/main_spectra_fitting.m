clear all; close all;
% See file "Absorbance_Fits.doc" for a description of this process.

% Set file path and name
filepath = '../../data/PEG10_DX10/(TAT)4_Hipco_PVP_B/';
filename = 'TAT4_HipCo_PVP_B_0uL';

% Laod data. col 1 is wavelength and col 2 is the absorbance. 
rawdata = load([filepath filename, '.txt']); 
% Load the initial parameters for each of the pure species.
par = load('parameters_DNA_coated.txt'); 

% wavelength measurement step (usually 0.5 or 1 nm)
wlspan = 1; 
% Set fitting range
fitstart = (1100-1070)/wlspan;     % End wavelength is 1100 nm
fitend = (1100-900)/wlspan;       % Start wavelength is 900 nm
wl = rawdata(fitstart:fitend,1);  % This is the range of wavelengths we want to fit
abs = rawdata(fitstart:fitend,2);  % This is the range of absorbance data we want to fit


% Plot the measured spectra
figure(2);
plot(wl,abs,'k','LineWidth',2.5);
xlabel('Wavelength (nm)','FontWeight','bold');
ylabel('Absorbance','FontWeight','bold');
title('Pure DNA initial scan fit','FontWeight','bold');
hold on;


% First, we specify our guess for coefficients. The last one is the
% baseline. Make this initial guess as close to measured data as possible
paramo = [0.10267 0.30743 0.7996 0.3143 0.0 0.0 .0 0.0 .0 .0 .0 .0 .309];
%      [9,1  8,3  6,5  7,5                                      baseline]

% Now we give the values from the parameters_DNA_coated.txt as our initial
% guess values for bl and br and peak.
bl= [par(:,6)' 1];
br= [par(:,7)' 1];
p = [par(:,1)' 1];

% Matrix init is our initial guess values matrix where 1st column is for 
% the fixed parameters, 2nd column for 'bl', 3rd column for 'br', and 4th 
% column for peak.
init = [paramo;bl;br;p]';
% We can turn more fixed parameters into free variables, such as amplitude
% 'a' etc by adding more inital guess value matrices here.


% Now we call 'combined', which will simply return the linear combination 
% of pure spectra using the parameters provided in 'paramo'.  
% Remember to change the values of beginx and endx in combined.m. 

beginp = 1; % First pure species to have variable coefficient in fit.
endp = 4;  % Last pure species to have variable coefficient in fit.
yfit = combined(init,wl);

% Plot the function as calculated by the initial fit.
% This gives us the pink spectra which is the initial guess plot
plot(wl,yfit,'m','LineWidth',2.5);


% Fit the free variables.
paramf = lsqcurvefit(@combined,init,wl',abs');

% Here the fitting is done. The free variables such as the coefficients, bl
% br, and p, are optimised till the difference between the measured spectra 
% (black plot) and the fitted spectra (red plot)is minimized.
yfit = combined(paramf,wl);

% Plot the fitting results
plot(wl,yfit,'r','LineWidth',2.5);

% Create the peak variable. 
peak = zeros(endp,2);

% Plot the individual peaks for each species.
for i = beginp:endp
    yfit2 = one_at_a_time(paramf(:,1)',[round(par(i,1)+34):-1:round(par(i,1)-34)]',i);
    plot([round(par(i,1)+34):-1:round(par(i,1)-34)]',yfit2,'LineWidth',1.2);
    peak(i,1) = par(i,1);
    peak(i,2) = yfit2(35);
end
legend ('Measured absorbance','Initial guess','Fit','Pure (9,1)','Pure (8,3)','Pure (6,5)','Pure (7,5)','FontWeight','bold');

% Save the fitting coefficients and peaks.
save([filepath, filename, '_coeff.txt'],'paramf','-ASCII');
save([filepath, filename, '_peak.txt'],'peak','-ASCII');

