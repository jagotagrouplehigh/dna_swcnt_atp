clear all
close all
clc

% Estimate the solubility parameter of the hybrids from partition
% coefficient by ATP experiments. 

%% constants
R = 8.314;      % gas constant (J/mol/K)
T = 298.15;     % room temperature (K)

% specific volumes (sv, mL/g)
sv_PEG = 0.832;
sv_DX = 0.626;
sv_PVP = 0.833;
sv_w = 1.00177;
sv_hybrid = 1;      % estimated value

% molecular weigth (MW, g/mol)
MW_PEG = 6000;
MW_DX = 70e3;
MW_PVP = 10e3;
MW_w = 18;

% solubility parameter (delta, MPa^0.5 -> Pa^0.5)
delta_PEG = 19.81*(1e6)^.5;
delta_DX = 38.6*(1e6)^.5;
delta_w = 47.8*(1e6)^.5;
delta_PVP = 24.3*(1e6)^.5;

% molar volumes (Vm, mL/mol -> m3/mol)
Vm_PEG = sv_PEG * MW_PEG * 1e-6;
Vm_DX = sv_DX * MW_DX * 1e-6;

% mass fraction % (x, w/w %) ~ mass (g) in 100 g of solution
x_PEG_t = 16;
x_DX_t = 0.2;   
x_PEG_b = 1;
x_DX_b = 27;
    
% Estimate molar volume of CNT
n = 6;
m = 5;
dia_cnt = 78.3e-3*sqrt((n+m)^2-n*m);    % diameter of CNT (nm)
d_cp = 0.592;       % distance from phosphate to CNT (nm) (d_cb = 0.365 nm)
dia_hybrid = dia_cnt + d_cp*2;
L_cnt = 200;        % CNT length (nm)
NAvo = 6.02e23;
Vi = pi*(dia_hybrid/2*1e-9)^2*L_cnt*1e-9*NAvo;    % molar volume (m3/mol)

%% read data files

% Set variable name
% Make sure that the variable names match with the data files (*.txt)

seq = {'TAT3T','TAT3TA','TAT4','GT15','AT15','TA15','GC15','CT15',...
    'GA15','AG15','CCA10','CA15','AC15'};
% seq = {'TAT3T','TAT4','GT15','GC15','AC15'};

filePath = '../data/PEG10_DX10/6_5_PEG10_DX10/';
file=dir([filePath, '*.txt']);

N=numel(file);
for i=1:N
    s=strrep(file(i).name,'.txt','');
    S{i}=s;
end

for i=1:N
    temp_lnK = load([filePath, file(i).name]);
    data.(seq{i}).rawmPVP = exp(temp_lnK(:,1));     % mass (mg) of PVP in actual solution
    temp_lnK(:,1) = exp(temp_lnK(:,1))*1e-3/0.5;    % PVP concentration (g/mL)
    temp_lnK(:,3) = temp_lnK(:,1)*100;              % mass (g) of PVP in 100 g of solution 
    data.(seq{i}).lnK = temp_lnK(:,2);
    data.(seq{i}).CPVP = temp_lnK(:,1);
    data.(seq{i}).mPVP = temp_lnK(:,3);
end

%% calculate parameters


for i = 1:N
    for j = 1:length(data.(seq{i}).lnK)
        x_w_t = 100 - data.(seq{i}).mPVP(j)/2 - x_PEG_t - x_DX_t;
        x_w_b = 100 - data.(seq{i}).mPVP(j)/2 - x_PEG_b - x_DX_b;
        % calculate volume fraction (phi, v/v %)
        data.(seq{i}).phi_PEG_t(j) = (x_PEG_t*sv_PEG) / (x_PEG_t*sv_PEG + x_DX_t*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_t*sv_w);
        data.(seq{i}).phi_DX_t(j) = (x_DX_t*sv_DX) / (x_PEG_t*sv_PEG + x_DX_t*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_t*sv_w);
        data.(seq{i}).phi_w_t(j) = (x_w_t*sv_w) / (x_PEG_t*sv_PEG + x_DX_t*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_t*sv_w);
        data.(seq{i}).phi_PVP_t(j) = (data.(seq{i}).mPVP(j)/2*sv_PVP) / (x_PEG_t*sv_PEG + x_DX_t*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_t*sv_w);
        data.(seq{i}).phi_PEG_b(j) = (x_PEG_b*sv_PEG) / (x_PEG_b*sv_PEG + x_DX_b*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_b*sv_w);
        data.(seq{i}).phi_DX_b(j) = (x_DX_b*sv_DX) / (x_PEG_b*sv_PEG + x_DX_b*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_b*sv_w);
        data.(seq{i}).phi_w_b(j) = (x_w_b*sv_w) / (x_PEG_b*sv_PEG + x_DX_b*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_b*sv_w);
        data.(seq{i}).phi_PVP_b(j) = (data.(seq{i}).mPVP(j)/2*sv_PVP) / (x_PEG_b*sv_PEG + x_DX_b*sv_DX ... 
            + data.(seq{i}).mPVP(j)/2*sv_PVP + x_w_b*sv_w);
        
        % calculate mole fraction
        kai_PEG_t = (x_PEG_t/MW_PEG) / (x_PEG_t/MW_PEG + x_DX_t/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_t/MW_w);
        kai_DX_t = (x_DX_t/MW_DX) / (x_PEG_t/MW_PEG + x_DX_t/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_t/MW_w);
        kai_w_t = (x_w_t/MW_w) / (x_PEG_t/MW_PEG + x_DX_t/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_t/MW_w);
        kai_PVP_t = (data.(seq{i}).mPVP(j)/2/MW_PVP) / (x_PEG_t/MW_PEG + x_DX_t/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_t/MW_w);
        kai_PEG_b = (x_PEG_b/MW_PEG) / (x_PEG_b/MW_PEG + x_DX_b/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_b/MW_w);
        kai_DX_b = (x_DX_b/MW_DX) / (x_PEG_b/MW_PEG + x_DX_b/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_b/MW_w);
        kai_w_b = (x_w_b/MW_w) / (x_PEG_b/MW_PEG + x_DX_b/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_b/MW_w);
        kai_PVP_b = (data.(seq{i}).mPVP(j)/2/MW_PVP) / (x_PEG_b/MW_PEG + x_DX_b/MW_DX ... 
            + data.(seq{i}).mPVP(j)/2/MW_PVP + x_w_b/MW_w);

        % molar volume of top and bottom phase (m3/mol)
        data.(seq{i}).Vm_t(j) = kai_PEG_t*Vm_PEG + kai_DX_t*Vm_DX + kai_w_t*sv_w*1e-6*MW_w ...
        + kai_PVP_t*sv_PVP*1e-6*MW_PVP;
        data.(seq{i}).Vm_b(j) = kai_PEG_b*Vm_PEG + kai_DX_b*Vm_DX + kai_w_b*sv_w*1e-6*MW_w ...
        + kai_PVP_b*sv_PVP*1e-6*MW_PVP;    
        
    end  
    
    % estimate the solubility parameters
    temp_lnVt_Vb = log(data.(seq{i}).Vm_t./data.(seq{i}).Vm_b);
    tempA = (data.(seq{i}).phi_PEG_t*delta_PEG + ...
        data.(seq{i}).phi_DX_t*delta_DX + data.(seq{i}).phi_w_t*delta_w).^2 ...
        - (data.(seq{i}).phi_PEG_b*delta_PEG + ...
        data.(seq{i}).phi_DX_b*delta_DX + data.(seq{i}).phi_w_b*delta_w).^2;
    tempB = data.(seq{i}).phi_PEG_t*delta_PEG ...
        + data.(seq{i}).phi_DX_t*delta_DX + data.(seq{i}).phi_w_t*delta_w ...
        - (data.(seq{i}).phi_PEG_b*delta_PEG ...
        + data.(seq{i}).phi_DX_b*delta_DX + data.(seq{i}).phi_w_b*delta_w);
    
    % solubility parameters (MPa^0.5)
    data.(seq{i}).delta_i = ((tempA/2./tempB + data.(seq{i}).phi_PVP_b*delta_PVP ...
        - (data.(seq{i}).lnK' - temp_lnVt_Vb)*R*T/2./Vi./tempB)*(1e-6)^.5)';
    
    % single value of the solubility parameter
    A = 2*tempB;
    B = (tempA + 2*tempB.*data.(seq{i}).phi_PVP_b*delta_PVP ...
        - (data.(seq{i}).lnK' - temp_lnVt_Vb)*R*T/Vi);
    data.(seq{i}).delta_i_ = A'\B'*(1e-6)^.5;
    
    % calculate the solubility parameter in bottom and top phase
    data.(seq{i}).delta_t = (data.(seq{i}).phi_PEG_t*delta_PEG + ...
        data.(seq{i}).phi_DX_t*delta_DX + data.(seq{i}).phi_w_t*delta_w ...
        + data.(seq{i}).phi_PVP_t*delta_PVP)*(1e-6)^.5;
    
    data.(seq{i}).delta_t_noPVP = (data.(seq{i}).phi_PEG_t*delta_PEG + ...
        data.(seq{i}).phi_DX_t*delta_DX + data.(seq{i}).phi_w_t*delta_w)*(1e-6)^.5;
        
    data.(seq{i}).delta_b = (data.(seq{i}).phi_PEG_b*delta_PEG + ...
        data.(seq{i}).phi_DX_b*delta_DX + data.(seq{i}).phi_w_b*delta_w ...
        + data.(seq{i}).phi_PVP_b*delta_PVP)*(1e-6)^.5;
    
    data.(seq{i}).delta_b_noPVP = (data.(seq{i}).phi_PEG_b*delta_PEG + ...
        data.(seq{i}).phi_DX_b*delta_DX + data.(seq{i}).phi_w_b*delta_w)*(1e-6)^.5;
   
    
end

for i = 1:N
    plot(data.(seq{i}).CPVP,data.(seq{i}).delta_i,'o')
    hold on;
end
xlabel('C_P_V_P (g/mL)')
ylabel('\delta_i (MPa^0^.^5)')
legend(seq,'Location','best')

PVPspan = 0:0.000001:0.002;
for i = 1:N
    fitpar(i,:) = polyfit(data.(seq{i}).CPVP,(data.(seq{i}).delta_i),1);
    % If you want to plot the fitting line:
%     semilogx(PVPspan,fitpar(i,1)*PVPspan+fitpar(i,2))
%     hold on;
end

hold off;

% Plot the slopes of each fitting line
figure(2)
bar(fitpar(:,1)*1e-3+fitpar(:,2)-44.75,'k')
ylabel('\delta_i at [PVP]=0.5 mg (MPa^0^.^5)')
set(gca, 'XTick', 1:length(fitpar(:,1)), 'XTickLabel', seq);
set(gca, 'YTick', [0:0.05:1], 'YTickLabel',[0:0.05:1]+44.75);

% Plot the intercepts of each fitting line
figure(3)
bar(fitpar(:,2)-44.886,'k')
ylabel('\delta_i at [PVP]=0.5 mg (MPa^0^.^5)')
set(gca, 'XTick', 1:length(fitpar(:,1)), 'XTickLabel', seq);
set(gca, 'YTick', [0:0.001:1], 'YTickLabel',[0:0.001:1]+44.886);


% Plot delta_t and delta_b
figure(4)
for i = 1:N
    plot(data.(seq{i}).CPVP,data.(seq{i}).delta_t,'o',data.(seq{i}).CPVP,data.(seq{i}).delta_b,'x')
    delta_t_mean_temp(i,1) = mean(data.(seq{i}).delta_t);
    delta_b_mean_temp(i,1) = mean(data.(seq{i}).delta_b);
    delta_t_noPVP_mean_temp(i,1) = mean(data.(seq{i}).delta_t_noPVP);
    delta_b_noPVP_mean_temp(i,1) = mean(data.(seq{i}).delta_b_noPVP);
    hold on;
end
xlabel('C_P_V_P (g/mL)')
ylabel('\delta_i (MPa^0^.^5)')
legend(seq,'Location','best')

% Calculate the mean of delta_t and delta_b
delta_t_mean = mean(delta_t_mean_temp);
delta_b_mean = mean(delta_b_mean_temp);
delta_t_noPVP_mean = mean(delta_t_noPVP_mean_temp);
delta_b_noPVP_mean = mean(delta_b_noPVP_mean_temp);
